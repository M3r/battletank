#include "BattleTankAutomatedTests.h"

#include "Misc/AutomationTest.h"
#include "Tests/AutomationCommon.h"
#include "Engine.h"
#include "EngineUtils.h"

#include "BattleTankGameModeBase.h"
#include "Tank.h"

/*
*   Author: Damian Pacholak
*   Description: Test checks if at least one AI has loaded at the test world map
*   Created: 11.08.2019 
*/


UWorld* GetTestWorld() {
    const TIndirectArray<FWorldContext>& WorldContexts = GEngine->GetWorldContexts();
    for (const FWorldContext& Context : WorldContexts) {
        if (((Context.WorldType == EWorldType::PIE) || (Context.WorldType == EWorldType::Game))
            && (Context.World() != nullptr)) {
            return Context.World();
        }
    }

    return nullptr;
}

IMPLEMENT_SIMPLE_AUTOMATION_TEST(IsAILoadedTest, "BattleTankTests.Placement.IsAiLoadedTest", EAutomationTestFlags::EditorContext | EAutomationTestFlags::EngineFilter)

    bool IsAILoadedTest::RunTest(const FString& Parameters) {
    AutomationOpenMap(TEXT("/Game/Maps/BattleGround"));

    UWorld* world = GetTestWorld();

    TestTrue("GameMode class is set correctly", world->GetAuthGameMode()->IsA<ABattleTankGameModeBase>());

    int32 numberOfActors = 0;

    for (TActorIterator<ATank> ActorItr(world); ActorItr; ++ActorItr)
    {
        numberOfActors++;
    }

    TestTrue("At least five Atank actors are present on map", (numberOfActors > 5));

    return true;
}