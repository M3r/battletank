// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/NavMovementComponent.h"

#include "TankMovementComponent.generated.h"

class UTankTrack;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()

public:
    UTankMovementComponent();

    UFUNCTION(BlueprintCallable, Category = "Setup")
    void Initialise(UTankTrack* leftTrackToSet, UTankTrack* rightTrackToSet);

    UFUNCTION(BlueprintCallable, Category="Input")
    void IntendMoveForward(float stickThrow);

    UFUNCTION(BlueprintCallable, Category = "Input")
    void IntendMoveRight(float stickThrow);

    virtual void RequestDirectMove(const FVector& moveVelocity, bool bForceMaxSpeed) override;

private:
    UTankTrack* tankTrackLeft = nullptr;
    UTankTrack* tankTrackRight = nullptr;

};
