// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "Components/StaticMeshComponent.h"
#include "Runtime/Core/Public/Math/UnrealMathUtility.h"

#include "TankBarrel.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
    UTankBarrel();

    void Elevate(float relativeSpeed);

private:
    UPROPERTY(EditDefaultsOnly, Category = "Setup")
    float maxDegreesPerSecond = 15.0f;

    UPROPERTY(EditDefaultsOnly, Category = "Setup")
    float maxElevationDegrees = 30.0f;

    UPROPERTY(EditDefaultsOnly, Category = "Setup")
    float minElevationDegrees = -1.0f;
	
};
