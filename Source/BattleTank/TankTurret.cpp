// M3r prv

#include "TankTurret.h"

UTankTurret::UTankTurret()
{
    PrimaryComponentTick.bCanEverTick = false;

}

void UTankTurret::Rotate(float relativeSpeed)
{
    relativeSpeed = FMath::Clamp<float>(relativeSpeed, -1, 1);

    auto rotationChange = relativeSpeed * maxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
    auto rawNewRotation = RelativeRotation.Yaw + rotationChange;
    auto rotation = rawNewRotation;
    SetRelativeRotation(FRotator(0, rotation, 0));

}
