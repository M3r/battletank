// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "Components/ActorComponent.h"
#include "GameFramework/Pawn.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Runtime/Engine/Classes/Engine/World.h"

#include "TankAimingComponent.generated.h"

UENUM()
enum class EFiringStatus : uint8
{
    Locked,
    Aiming,
    Reloading,
    OutOfAmmo
};

class UTankBarrel;
class UTankTurret;
class AProjectile;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

    EFiringStatus GetFiringStatus() const { return firingStatus; }

    UFUNCTION(BlueprintCallable, Category = "Firing")
    int32 GetCurrentAmmo() const { return currentAmmo; }

    UFUNCTION(BlueprintCallable, Category = "Setup")
    void Initialise(UTankBarrel* barrelToSet, UTankTurret* turretToSet);

    UFUNCTION(BlueprintCallable, Category = "Setup")
    void SetProjectileBlueprint(TSubclassOf<AProjectile> projectileToSet);

    void MoveBarrelAtTarget(FVector aimDirection);

    void MoveTurretAtTarget(FVector aimDirection);

    void AimAt(FVector hitLocation);

    UFUNCTION(BlueprintCallable, Category = "Firing")
    void Fire();

protected:
    UPROPERTY(BlueprintReadOnly, Category = "State")
    EFiringStatus firingStatus = EFiringStatus::Reloading;

private:
    UTankBarrel* barrel = nullptr;

    UTankTurret* turret = nullptr;

    UPROPERTY(EditDefaultsOnly, Category = "Firing")
    int32 currentAmmo = 666;

    UPROPERTY(EditDefaultsOnly, Category = "Firing")
    float launchSpeed = 8000.0f;

    UPROPERTY(EditDefaultsOnly, Category = "Setup")
    TSubclassOf<AProjectile> projectileBlueprint = nullptr;

    UPROPERTY(EditAnywhere, Category = "Firing")
    float reloadTime = 3.0f;

    double lastFireTime = 0;

    FVector aimDirection;

    void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

    void BeginPlay() override;

    bool IsBarrelMoving();
};
