// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Components/SceneComponent.h"

#include "SpawnPoint.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API USpawnPoint : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USpawnPoint();

    AActor* GetSpawnedActor() const { return spawnedActor; };

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
    UPROPERTY(EditDefaultsOnly, Category = "Setup")
    TSubclassOf<AActor> spawnClass;

    UPROPERTY()
    AActor* spawnedActor;

};
