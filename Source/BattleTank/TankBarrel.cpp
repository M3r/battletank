// M3r prv

#include "TankBarrel.h"

UTankBarrel::UTankBarrel()
{
    // Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
    // off to improve performance if you don't need them.
    PrimaryComponentTick.bCanEverTick = false;

    // ...
}

void UTankBarrel::Elevate(float relativeSpeed)
{
    relativeSpeed = FMath::Clamp<float>(relativeSpeed, -1, 1);

    auto elevationChange = relativeSpeed * maxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
    auto rawNewElevation = RelativeRotation.Pitch + elevationChange;
    auto elevation = FMath::Clamp<float>(rawNewElevation, minElevationDegrees, maxElevationDegrees);

    SetRelativeRotation(FRotator(elevation, 0, 0));

}
