// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "Runtime/Engine/Classes/Engine/World.h"
#include "GameFramework/PlayerController.h"

#include "TankPlayerController.generated.h"

class UTankAimingComponent;

UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
    GENERATED_BODY()

public:
    ATankPlayerController();

    UPROPERTY(EditDefaultsOnly)
    float crosshairXLocation = 0.5f;

    UPROPERTY(EditDefaultsOnly)
    float crosshairYLocation = 0.3333f;

    UPROPERTY(EditDefaultsOnly)
    float lineTraceRange = 1000000.0f;

protected:
    UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
    void FoundAimingComponent(UTankAimingComponent* aimingComponentReference);

private:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    virtual void Tick(float DeltaTime) override;

    void AimTowardsCrosshair();

    bool GetSightRayHitLocation(FVector& HitLocation) const;

    bool GetLookDirection(FVector2D screenLocation, FVector& lookDirection) const;

    bool GetLookVectorHitLocation(FVector lookDirection, FVector& hitLocation) const;

};
