// M3r prv


#include "SprungWheel.h"

#include "Runtime/Engine/Classes/Components/SphereComponent.h"

// Sets default values
ASprungWheel::ASprungWheel()
{
    // Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.TickGroup = TG_PostPhysics;

    physicsConstraint = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("Physics Constraint"));
    SetRootComponent(physicsConstraint);

    physicsConstraint->SetLinearXLimit(ELinearConstraintMotion::LCM_Locked, 100);
    physicsConstraint->SetLinearYLimit(ELinearConstraintMotion::LCM_Locked, 100);
    physicsConstraint->SetLinearZLimit(ELinearConstraintMotion::LCM_Free, 100);

    physicsConstraint->SetAngularSwing1Limit(EAngularConstraintMotion::ACM_Locked, 45);
    physicsConstraint->SetAngularSwing2Limit(EAngularConstraintMotion::ACM_Locked, 45);
    physicsConstraint->SetAngularTwistLimit(EAngularConstraintMotion::ACM_Locked, 45);

    physicsConstraint->SetLinearPositionTarget(FVector(0, 0, 0));
    physicsConstraint->SetLinearPositionDrive(false, false, true);

    physicsConstraint->SetLinearVelocityTarget(FVector(0, 0, 0));
    physicsConstraint->SetLinearVelocityDrive(false, false, true);

    physicsConstraint->SetLinearDriveParams(5000, 2000, 0);

    axle = CreateDefaultSubobject<USphereComponent>(FName("axle"));
    axle->SetupAttachment(physicsConstraint);

    physicsConstraintAxle = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("Physics Axle Constraint"));
    physicsConstraintAxle->SetupAttachment(axle);

    wheel = CreateDefaultSubobject<USphereComponent>(FName("wheel"));
    wheel->SetupAttachment(axle);

    wheel->SetSimulatePhysics(true);
    wheel->SetMassOverrideInKg(FName(""), 100.0f);

}


// Called when the game starts or when spawned
void ASprungWheel::BeginPlay()
{
	Super::BeginPlay();

    wheel->SetNotifyRigidBodyCollision(true);
    wheel->OnComponentHit.AddDynamic(this, &ASprungWheel::OnHit);

    if (!ensure(GetAttachParentActor())) return;
    UPrimitiveComponent* bodyRoot = Cast<UPrimitiveComponent>(GetAttachParentActor()->GetRootComponent());
    if (!ensure(bodyRoot)) return;
    physicsConstraint->SetConstrainedComponents(bodyRoot, NAME_None, axle, NAME_None);
    physicsConstraintAxle->SetConstrainedComponents(axle, NAME_None, wheel, NAME_None);

}

// Called every frame
void ASprungWheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

    if (GetWorld()->TickGroup == TG_PostPhysics)
    {
        totalForceMagnitudeThisFrame = 0;
    }

}

void ASprungWheel::AddDrivingForce(float forceMagnitude)
{
    totalForceMagnitudeThisFrame += forceMagnitude;

}

void ASprungWheel::OnHit
(
    UPrimitiveComponent* HitComponent, 
    AActor* OtherActor, 
    UPrimitiveComponent* OtherComp, 
    FVector NormalImpulse, 
    const FHitResult& Hit
)
{
    ApplyForce();

}

void ASprungWheel::ApplyForce()
{
    wheel->AddForce(axle->GetForwardVector() * totalForceMagnitudeThisFrame);
    return;

}

