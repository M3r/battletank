// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"

#include "Projectile.generated.h"

class UProjectileMovementComponent;

// TODO: Reduce projectile impulse strength

UCLASS()
class BATTLETANK_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();

    void Launch(float speed);

private:	
    UProjectileMovementComponent* projectileMovementComponent = nullptr;

};
