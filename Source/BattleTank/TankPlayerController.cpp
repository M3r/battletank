// M3r prv

#include "TankPlayerController.h"

#include "TankAimingComponent.h"

ATankPlayerController::ATankPlayerController()
{
    // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATankPlayerController::BeginPlay()
{
    Super::BeginPlay();

    auto aimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
    if (!ensureMsgf(aimingComponent, TEXT("Aiming component not found on begin play. Possible race condition issue."))) return;
    if (aimingComponent) FoundAimingComponent(aimingComponent);

}

// Called every frame
void ATankPlayerController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    AimTowardsCrosshair();

}

void ATankPlayerController::AimTowardsCrosshair()
{
    auto aimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
    if (!ensureMsgf(aimingComponent, TEXT("Aiming component not found on begin play. Possible race condition issue."))) return;

    FVector hitLocation;
    if (GetSightRayHitLocation(hitLocation))
    {
        aimingComponent->AimAt(hitLocation);

    }

}

bool ATankPlayerController::GetSightRayHitLocation(FVector& hitLocation) const
{
    int32 viewportSizeX, viewportSizeY;
    GetViewportSize(viewportSizeX, viewportSizeY);
    FVector2D crosshairScreenPosition(viewportSizeX * crosshairXLocation, viewportSizeY * crosshairYLocation);

    FHitResult HitResult;
    if (GetHitResultAtScreenPosition(crosshairScreenPosition, ECC_Visibility, false, OUT HitResult))
    {
        hitLocation = HitResult.Location;
        return true;
    }
    // if we cant find some physical body take abstract distant point in the sky passing through camera and crosshair
    else
    {
        FVector cameraLocation;
        FVector cameraDirection;
        DeprojectScreenPositionToWorld(crosshairScreenPosition.X, crosshairScreenPosition.Y, cameraLocation, cameraDirection);
        hitLocation = FVector(cameraLocation + cameraDirection * 10000);
        return true;

    }

    return false;

}


bool ATankPlayerController::GetLookDirection(FVector2D screenLocation, FVector& lookDirection) const
{
    FVector cameraWorldLocation; // to be discarded
    return DeprojectScreenPositionToWorld(screenLocation.X, screenLocation.Y, cameraWorldLocation, lookDirection);

}

bool ATankPlayerController::GetLookVectorHitLocation(FVector lookDirection, FVector& hitLocation) const
{
    FHitResult hitResult;
    FVector startLocation = PlayerCameraManager->GetCameraLocation();
    auto endLocation = startLocation + (lookDirection * lineTraceRange);
    if (GetWorld()->LineTraceSingleByChannel(hitResult, startLocation, endLocation, ECollisionChannel::ECC_Visibility))
    {
        hitLocation = hitResult.Location;
        return true;
    }

    hitLocation = FVector(0);
    return false;

}