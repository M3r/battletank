// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "Components/StaticMeshComponent.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectBaseUtility.h"

#include "TankTrack.generated.h"

class ASprungWheel;

UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
    UTankTrack();

    UFUNCTION(BlueprintCallable, Category = "Input")
    void SetThrottle(float throttle);

private:
    // Max force per track in [N]
    UPROPERTY(EditDefaultsOnly)
    float maxDrivingForce = 40000000.0f;

    TArray<class ASprungWheel*> GetWheels() const;

};
