// M3r prv

#pragma once

#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/Controller.h"
#include "Runtime/Engine/Classes/Engine/LocalPlayer.h"
#include "CoreMinimal.h"
#include "AIController.h"

#include "TankAIController.generated.h"

UCLASS()
class BATTLETANK_API ATankAIController : public AAIController
{
	GENERATED_BODY()

public:
    ATankAIController();
	
protected:
    virtual void Tick(float DeltaTime) override;

    UPROPERTY(EditDefaultsOnly, Category = "Setup")
    float acceptanceRadious = 5000.0f;

private:


};
