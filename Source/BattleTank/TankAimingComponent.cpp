// M3r prv

#include "TankAimingComponent.h"

#include "TankBarrel.h"
#include "TankTurret.h"
#include "Projectile.h"

// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

}

void UTankAimingComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    if (!currentAmmo)
    {
        firingStatus = EFiringStatus::OutOfAmmo;
    }
    else if (FPlatformTime::Seconds() - lastFireTime < reloadTime)
    {
        firingStatus = EFiringStatus::Reloading;
    }
    else if (IsBarrelMoving())
    {
        firingStatus = EFiringStatus::Aiming;
    }
    else
    {
        firingStatus = EFiringStatus::Locked;
    }
}

void UTankAimingComponent::BeginPlay()
{
    Super::BeginPlay();

    lastFireTime = FPlatformTime::Seconds();

}

bool UTankAimingComponent::IsBarrelMoving()
{
    if (!ensure(barrel)) { return false; }

    auto barrelForward = barrel->GetForwardVector();

    return !barrelForward.Equals(aimDirection, 0.05f);

}

void UTankAimingComponent::Initialise(UTankBarrel* barrelToSet, UTankTurret* turretToSet)
{
    barrel = barrelToSet;
    turret = turretToSet;

}

void UTankAimingComponent::AimAt(FVector hitLocation)
{
    if (!ensure(barrel)) return;

    FVector outLaunchVelocity;
    FVector startLocaton = barrel->GetSocketLocation(FName("projectileStartLocation"));
    TArray<AActor*> actorsToIgnore;
    bool bHaveAimSolution = UGameplayStatics::SuggestProjectileVelocity(
            this,
            outLaunchVelocity,
            startLocaton,
            hitLocation,
            launchSpeed,
            false,
            0,
            0,
            ESuggestProjVelocityTraceOption::DoNotTrace,
            FCollisionResponseParams::DefaultResponseParam,
            actorsToIgnore,
            false
        );

    if (bHaveAimSolution)
    {
        aimDirection = outLaunchVelocity.GetSafeNormal();

        MoveBarrelAtTarget(aimDirection);
        MoveTurretAtTarget(aimDirection);
    }

}

void UTankAimingComponent::SetProjectileBlueprint(TSubclassOf<AProjectile> projectileToSet)
{
    projectileBlueprint = projectileToSet;
}

void UTankAimingComponent::MoveBarrelAtTarget(FVector aimDirection)
{
    auto barrelRotator = barrel->GetForwardVector().Rotation();
    auto aimAsRotator = aimDirection.Rotation();
    auto deltaRotator = aimAsRotator - barrelRotator;

    barrel->Elevate(deltaRotator.GetNormalized().Pitch); 

}

void UTankAimingComponent::MoveTurretAtTarget(FVector aimDirection)
{
    auto turretRotater = turret->GetForwardVector().Rotation();
    auto aimAsRotator = aimDirection.Rotation();
    auto deltaRotator = aimAsRotator - turretRotater;

    turret->Rotate(deltaRotator.GetNormalized().Yaw);

}

void UTankAimingComponent::Fire()
{
    if (!ensure(barrel && projectileBlueprint)) { return; }

    if (firingStatus != EFiringStatus::Reloading && firingStatus != EFiringStatus::OutOfAmmo)
    {
        auto projectile = GetWorld()->SpawnActor<AProjectile>
            (
                projectileBlueprint,
                barrel->GetSocketLocation(FName("projectileStartLocation")),
                barrel->GetSocketRotation(FName("projectileStartLocation"))
            );
        projectile->Launch(launchSpeed);
        currentAmmo--;

        lastFireTime = FPlatformTime::Seconds();
    }
}