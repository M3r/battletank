// M3r prv

#pragma once

#include "CoreMinimal.h"

#include "Runtime/Engine/Classes/PhysicsEngine/PhysicsConstraintComponent.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "GameFramework/Actor.h"

#include "SprungWheel.generated.h"

class USphereComponent;

UCLASS()
class BATTLETANK_API ASprungWheel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASprungWheel();

    // Called every frame
    virtual void Tick(float DeltaTime) override;

    void AddDrivingForce(float forceMagnitude);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
    UPROPERTY(VisibleAnywhere, Category = "Setup")
    USphereComponent* wheel = nullptr;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UPhysicsConstraintComponent* physicsConstraint = nullptr;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    USphereComponent* axle = nullptr;

    UPROPERTY(VisibleAnywhere, Category = "Setup")
    UPhysicsConstraintComponent* physicsConstraintAxle = nullptr;

    float totalForceMagnitudeThisFrame = 0;


    UFUNCTION()
    void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

    void ApplyForce();

};
