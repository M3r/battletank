// M3r prv

#include "TankTrack.h"

#include "SprungWheel.h"
#include "SpawnPoint.h"

UTankTrack::UTankTrack()
{
    PrimaryComponentTick.bCanEverTick = false;

}

void UTankTrack::SetThrottle(float throttle)
{
    auto currentThrottle = FMath::Clamp<float>(throttle, -1.0f, 1.0f);
    auto forceApplied = currentThrottle * maxDrivingForce;
    auto wheels = GetWheels();
    auto forcePerWheel = forceApplied / wheels.Num();
    for (ASprungWheel* wheel : wheels)
    {
        wheel->AddDrivingForce(forcePerWheel);
    }

}

TArray<class ASprungWheel*> UTankTrack::GetWheels() const
{
    TArray<ASprungWheel*> resultArray;
    TArray<USceneComponent*> children;
    GetChildrenComponents(true, children);
    for (USceneComponent* child : children)
    {
        auto spawnPointChild = Cast<USpawnPoint>(child);
        if (!spawnPointChild) continue;

        AActor* spawnedChild = spawnPointChild->GetSpawnedActor();
        auto sprungWheel = Cast<ASprungWheel>(spawnedChild);
        if (!sprungWheel) continue;
        resultArray.Add(sprungWheel);
    }
    return resultArray; 

}

