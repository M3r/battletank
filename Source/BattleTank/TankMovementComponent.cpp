// M3r prv

#include "TankMovementComponent.h"

#include "TankTrack.h"

UTankMovementComponent::UTankMovementComponent()
{
    PrimaryComponentTick.bCanEverTick = false;

}

void UTankMovementComponent::Initialise(UTankTrack* leftTrackToSet, UTankTrack* rightTrackToSet)
{
    tankTrackLeft = leftTrackToSet;
    tankTrackRight = rightTrackToSet;

}

void UTankMovementComponent::IntendMoveForward(float moveThrow)
{
    if (!ensure(tankTrackLeft && tankTrackRight)) { return; }
    tankTrackLeft->SetThrottle(moveThrow);
    tankTrackRight->SetThrottle(moveThrow);

}

void UTankMovementComponent::IntendMoveRight(float moveThrow)
{
    if (!ensure(tankTrackLeft && tankTrackRight)) { return; }
    tankTrackLeft->SetThrottle(moveThrow);
    tankTrackRight->SetThrottle(-moveThrow);

}

void UTankMovementComponent::RequestDirectMove(const FVector& moveVelocity, bool bForceMaxSpeed)
{
    auto tankForwadDirection = GetOwner()->GetActorForwardVector().GetSafeNormal();
    auto aiForwardIntention = moveVelocity.GetSafeNormal();

    auto forwardThrow = FVector::DotProduct(tankForwadDirection, aiForwardIntention);
    IntendMoveForward(forwardThrow);

    auto rightThrow = FVector::CrossProduct(tankForwadDirection, aiForwardIntention).Z;
    IntendMoveRight(rightThrow);

}

