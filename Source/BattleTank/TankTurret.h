// M3r prv

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTurret.generated.h"


UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
    UTankTurret();

    void Rotate(float relativeSpeed);

private:
    UPROPERTY(EditDefaultsOnly, Category = "Setup")
    float maxDegreesPerSecond = 40.0f;

};
