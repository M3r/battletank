// M3r prv

#include "TankAIController.h"

#include "TankAimingComponent.h"

ATankAIController::ATankAIController()
{
    PrimaryActorTick.bCanEverTick = true;

}


void ATankAIController::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);

    auto playerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
    auto controlledTank = GetPawn();

    // Aim towards the player
    if (!ensure(playerTank && controlledTank)) return;

    // Move towards player
    MoveToActor(playerTank, acceptanceRadious);
     
    auto tankAimingComponent = controlledTank->FindComponentByClass<UTankAimingComponent>();

    tankAimingComponent->AimAt(playerTank->GetActorLocation());

    if (tankAimingComponent->GetFiringStatus() == EFiringStatus::Locked)
    {
        tankAimingComponent->Fire();

    }
}